package org.openstreetmap.josm.plugins.sourcetagupdater;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;
import org.openstreetmap.josm.gui.MainApplication;

public class SourceTagUpdaterPlugin extends Plugin {

    /**
     * Will be invoked by JOSM to bootstrap the plugin
     *
     * @param info  information about the plugin and its local installation    
     */
    public SourceTagUpdaterPlugin(PluginInformation info) {
        super(info);
        SourceTagUpdaterLayerChangeListener layerChangeListener = new SourceTagUpdaterLayerChangeListener();
        MainApplication.getLayerManager().addLayerChangeListener(layerChangeListener);
    }
}
