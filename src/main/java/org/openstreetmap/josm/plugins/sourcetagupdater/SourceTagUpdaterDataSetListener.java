package org.openstreetmap.josm.plugins.sourcetagupdater;

import java.lang.AssertionError;
import java.util.HashSet;
import java.util.Set;

import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.event.DataSetListener;
import org.openstreetmap.josm.data.osm.event.AbstractDatasetChangedEvent;
import org.openstreetmap.josm.data.osm.event.AbstractDatasetChangedEvent.*;
import org.openstreetmap.josm.data.osm.event.DataChangedEvent;
import org.openstreetmap.josm.data.osm.event.DataSetListener;
import org.openstreetmap.josm.data.osm.event.NodeMovedEvent;
import org.openstreetmap.josm.data.osm.event.PrimitivesAddedEvent;
import org.openstreetmap.josm.data.osm.event.PrimitivesRemovedEvent;
import org.openstreetmap.josm.data.osm.event.RelationMembersChangedEvent;
import org.openstreetmap.josm.data.osm.event.TagsChangedEvent;
import org.openstreetmap.josm.data.osm.event.WayNodesChangedEvent;
import org.openstreetmap.josm.gui.MainApplication;

public class SourceTagUpdaterDataSetListener implements DataSetListener {
    @Override
    public void primitivesAdded(PrimitivesAddedEvent event) {
        Set<String> sourceTagsToAdd = new HashSet<String>();
        event.getPrimitives().forEach(p -> {
            String sourceTag = p.get("source");
            if(sourceTag != null) {
                sourceTagsToAdd.add(sourceTag);
            }
        });
        for(String source : sourceTagsToAdd) {
            addSourceTagToChangeset(source);
        }
    }

    protected void addSourceTagToChangeset(String source) {
        String currentSource = MainApplication.getLayerManager().getEditDataSet().getChangeSetTags().get("source");
        if(currentSource == null) {
            MainApplication.getLayerManager().getEditDataSet().addChangeSetTag("source", source);
        } else if(!currentSource.contains(source)) {
            MainApplication.getLayerManager().getEditDataSet().addChangeSetTag("source", currentSource + ";" + source);
        }
    }

    @Override
    public void primitivesRemoved(PrimitivesRemovedEvent event) {
        // Nothing to be done here
    }

    protected void removeSourceTag(OsmPrimitive primitive) {
        new Thread(() -> {
            primitive.remove("source");
        }).start();
    }

    @Override
    public void tagsChanged(TagsChangedEvent event) {
        OsmPrimitive osmPrimitive = event.getPrimitive();
        // Don't remove source when source has been modified TODO
        if (osmPrimitive.get("source") == null
                || osmPrimitive.get("source") != event.getOriginalKeys().get("source")) {
            return;
        }
        removeSourceTag(osmPrimitive);
    }

    @Override
    public void nodeMoved(NodeMovedEvent event) {
        Node movedNode = event.getNode();
        removeSourceTag(movedNode);
        for (OsmPrimitive parentWay : movedNode.getParentWays()) {
            removeSourceTag(parentWay);
        }
    }

    @Override
    public void wayNodesChanged(WayNodesChangedEvent event) {
        removeSourceTag(event.getChangedWay());
    }

    @Override
    public void relationMembersChanged(RelationMembersChangedEvent event) {
        removeSourceTag(event.getRelation());
    }

    /**
     * @see DataSetListener.otherDatasetChange
     */
    @Override
    public void otherDatasetChange(AbstractDatasetChangedEvent event) {
        // Nothing to be done here
    }

    @Override
    public void dataChanged(DataChangedEvent event) {
        event.getEvents();
    }
}
