package org.openstreetmap.josm.plugins.sourcetagupdater;

import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.gui.layer.AbstractOsmDataLayer;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerAddEvent;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerChangeListener;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerOrderChangeEvent;
import org.openstreetmap.josm.gui.layer.LayerManager.LayerRemoveEvent;

public class SourceTagUpdaterLayerChangeListener implements LayerChangeListener {
    private SourceTagUpdaterDataSetListener datasetListener;

    public SourceTagUpdaterLayerChangeListener() {
        datasetListener = new SourceTagUpdaterDataSetListener();
    }

    public void layerAdded(LayerAddEvent e) {
        Layer layer = e.getAddedLayer();
        if (layer instanceof OsmDataLayer) {
            DataSet ds = ((OsmDataLayer) layer).getDataSet();
            ds.addDataSetListener(datasetListener);
        }
    }
    public void layerRemoving(LayerRemoveEvent e) {
        Layer layer = e.getRemovedLayer();
        if (layer instanceof OsmDataLayer) {
            DataSet ds = ((OsmDataLayer) layer).getDataSet();
            ds.removeDataSetListener(datasetListener);
        }
    }
    public void layerOrderChanged(LayerOrderChangeEvent e) {
    }
}
